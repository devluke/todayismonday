<?php require_once "common.php" ?>

<?php

	// Check if the user is not logged in, if yes, redirect them to login page
	if (!isset($_SESSION["logged_in"]) || $_SESSION["logged_in"] === false) {

		redirect("login.php");

	}

?>