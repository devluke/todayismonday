<?php require_once "common.php" ?>

<?php

	// Check if the user is already logged in, if yes, redirect them to home page
	if (isset($_SESSION["logged_in"]) && $_SESSION["logged_in"] === true) {

		redirect("/");

	}

?>