<?php require_once "common.php" ?>

<?php

	// Initialize the session
	session_start();

	// Require config file
	require_once "config.php";

	// Define variables and initialize with empty values
	$email = $password = "";
	$email_err = $password_err = "";
	$error = false;

	// Processing form data when form is submitted
	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		// Check if email is empty
		if (empty(trim($_POST["email"]))) {

			$email_err = "Please enter your email";

		} elseif (!filter_var(trim($_POST["email"]), FILTER_VALIDATE_EMAIL)) {

			$email_err = "Please enter a valid email";

		} else {

			$email = trim($_POST["email"]);

		}

		// Check if password is empty
		if (empty(trim($_POST["password"]))) {

			$password_err = "Please enter your password";

		} else {

			$password = trim($_POST["password"]);

		}

		// Validate credentials
		if (empty($email_err) && empty($password_err)) {

			// Prepare a select statement
			$sql = "SELECT id, full_name, email, password, created_at FROM users WHERE email = ?";

			if ($stmt = mysqli_prepare($link, $sql)) {

				// Bind variables to the prepared statement as parameters
				mysqli_stmt_bind_param($stmt, "s", $param_email);

				// Set parameters
				$param_email = $email;

				// Attempt to execute the prepared statement
				if (mysqli_stmt_execute($stmt)) {

					// Store result
					mysqli_stmt_store_result($stmt);

					// Check if email exists, if yes then verify password
					if (mysqli_stmt_num_rows($stmt) == 1) {

						// Bind result variables
						mysqli_stmt_bind_result($stmt, $id, $full_name, $email, $hashed_password, $created_at);

						if (mysqli_stmt_fetch($stmt)) {

							if (password_verify($password, $hashed_password)) {

								// Password is correct, start a new session
								session_start();

								// Store data in session variables
								$_SESSION["logged_in"] = true;
								$_SESSION["id"] = $id;
								$_SESSION["full_name"] = $full_name;
								$_SESSION["email"] = $email;
								$_SESSION["created_at"] = $created_at;

								// Redirect user to home page
								redirect("/");

							} else {

								// Display an error message if password is not valid
								$error = true;

							}

						}

					} else {

						// Display an error message if email doesn't exist
						$error = true;

					}

				} else {

					echo "Something went wrong. Please try again later.";

				}

			}

			// Close statement
			mysqli_stmt_close($stmt);

		}

		// Close connection
		mysqli_close($link);

	}

?>

<?php require_once "guestpage.php" ?>

<!doctype html>
<html>

	<head>

		<!-- Require header -->
		<?php require_once "header.php"; ?>

	</head>

	<body>

		<!-- Require navbar -->
		<?php require_once "guestnavbar.php"; ?>

		<div class="container">

			<?php

				if ($error) {

					echo "
					
						<div class='alert alert-danger' role='alert'>

							Invalid email/password
				  
						</div>
					
					";

				}

			?>

			<h2>Login</h2>

			<p>Sign in to your account.</p>

			<hr>

			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

				<div class="form-group <?php echo (!empty($email_err)) ? "has-error" : ""; ?>">

					<b>Email</b>

					<input type="text" name="email" class="form-control" value="<?php echo $email; ?>">

					<span class="help-block" style="color: red;"><?php echo $email_err; ?></span>

				</div>

				<div class="form-group <?php echo (!empty($password_err)) ? "has-error" : ""; ?>">

					<b>Password</b>

					<input type="password" name="password" class="form-control" value="<?php echo $password; ?>">

					<span class="help-block" style="color: red;"><?php echo $password_err; ?></span>

				</div>

				<div class="form-group">

					<input type="submit" class="btn btn-primary" value="Login">

				</div>

				<p>Don't have an account? <a href="register.php">Sign up now</a>.</p>

			</form>

		</div>

		<!-- Require footer -->
		<?php require_once "footer.php"; ?>

	</body>

</html>