<nav class="navbar navbar-light bg-light">

	<div class="container">

		<a class="navbar-brand" href="/">Today is <?php echo date("l"); ?></a>

		<form class="form-inline">

			<div class="input-group">

				<div class="input-group-prepend">

					<span class="input-group-text">#</span>

				</div>

				<input type="number" class="form-control" id="groupId" placeholder="Group ID">

			</div>

		</form>

		<a class="navbar-text" href="#">Logged in as Luke Chambers</a>

	</div>

</nav>

<br>