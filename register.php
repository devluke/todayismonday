<?php require_once "common.php" ?>

<?php

	// Require config file
	require_once "config.php";

	// Define variables and initialize with empty values
	$full_name = $email = $password = $confirm_password = "";
	$full_name_err = $password_err = $confirm_password_err = "";

	// Processing form data when form is submitted
	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		// Validate full name
		if (empty(trim($_POST["full_name"]))) {

			$full_name_err = "Please enter a name";

		} else {

			$full_name = trim($_POST["full_name"]);

		}

		// Validate email
		if (empty(trim($_POST["email"]))) {

			$email_err = "Please enter an email";

		} elseif (!filter_var(trim($_POST["email"]), FILTER_VALIDATE_EMAIL)) {

			$email_err = "Please enter a valid email";

		} else {

			// Prepare a select statement
			$sql = "SELECT id FROM users WHERE email = ?";

			if ($stmt = mysqli_prepare($link, $sql)) {

				// Bind variables to the prepared statement as parameters
				mysqli_stmt_bind_param($stmt, "s", $param_email);

				// Set parameters
				$param_email = trim($_POST["email"]);

				// Attempt to execute the prepared statement
				if (mysqli_stmt_execute($stmt)) {

					// Store result
					mysqli_stmt_store_result($stmt);

					if (mysqli_stmt_num_rows($stmt) == 1) {

						$email_err = "This email has already been used";

					} else {

						$email = trim($_POST["email"]);

					}

				}

			}

			// Close statement
			mysqli_stmt_close($stmt);

		}

		// Validate password
		if (empty(trim($_POST["password"]))) {

			$password_err = "Please enter a password";

		} else if (strlen(trim($_POST["password"])) < 8) {

			$password_err = "Password must have at least 8 characters";

		} else {

			$password = trim($_POST["password"]);

		}

		// Validate confirm password
		if (empty(trim($_POST["confirm_password"]))) {

			$confirm_password_err = "Please confirm your password";

		} else {

			$confirm_password = trim($_POST["confirm_password"]);

			if (empty($password_err) && ($password != $confirm_password)) {

				$confirm_password_err = "Password does not match";

			}

		}

		// Check input errors before inserting in database
		if (empty($full_name_err) && empty($email_err) && empty($password_err) && empty($confirm_password_err)) {

			// Prepare an insert statement
			$sql = "INSERT INTO users (full_name, email, password) VALUES (?, ?, ?)";

			if ($stmt = mysqli_prepare($link, $sql)) {

				// Bind variables to the prepared statement as parameters
				mysqli_stmt_bind_param($stmt, "sss", $param_full_name, $param_email, $param_password);

				// Set parameters
				$param_full_name = trim($_POST["full_name"]);
				$param_email = trim($_POST["email"]);
				$param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash

				// Attempt to execute the prepared statement
				if (mysqli_stmt_execute($stmt)) {

					// Redirect to login page
					redirect("login.php");

				} else {
					
					echo "Something went wrong. Please try again later.";

				}

			}

			// Close statement
			mysqli_stmt_close($stmt);

		}

		// Close connection
		mysqli_close($link);

	}

?>

<?php require_once "guestpage.php" ?>

<!doctype html>
<html>

	<head>

		<!-- Require header -->
		<?php require_once "header.php"; ?>

	</head>

	<body>

		<!-- Require navbar -->
		<?php require_once "guestnavbar.php"; ?>

		<div class="container">

			<h2>Register</h2>

			<p>Create a free account.</p>

			<hr>

			<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

				<div class="form-group <?php echo (!empty($full_name_err)) ? "has-error" : ""; ?>">

					<b>Full Name</b>

					<input type="text" name="full_name" class="form-control" value="<?php echo $full_name; ?>">

					<span class="help-block" style="color: red;"><?php echo $full_name_err; ?></span>

				</div>

				<div class="form-group <?php echo (!empty($email_err)) ? "has-error" : ""; ?>">

					<b>Email</b>

					<input type="text" name="email" class="form-control" value="<?php echo $email; ?>">

					<span class="help-block" style="color: red;"><?php echo $email_err; ?></span>

				</div>

				<div class="form-group <?php echo (!empty($password_err)) ? "has-error" : ""; ?>">

					<b>Password</b>

					<input type="password" name="password" class="form-control" value="<?php echo $password; ?>">

					<span class="help-block" style="color: red;"><?php echo $password_err; ?></span>

				</div>

				<div class="form-group <?php echo (!empty($confirm_password_err)) ? "has-error" : ""; ?>">

					<b>Confirm Password</b>

					<input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">

					<span class="help-block" style="color: red;"><?php echo $confirm_password_err; ?></span>

				</div>

				<div class="form-group">

					<input type="submit" class="btn btn-primary" value="Register">

				</div>

				<p>Already have an account? <a href="login.php">Login here</a>.</p>

			</form>

		</div>

		<!-- Require footer -->
		<?php require_once "footer.php"; ?>

	</body>

</html>