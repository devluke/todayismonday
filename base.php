<?php require_once "common.php" ?>

<!doctype html>
<html>

	<head>

		<!-- Require header -->
		<?php require_once "header.php"; ?>

	</head>

	<body>

		<!-- Require navbar -->
		<?php require_once "navbar.php"; ?>

		<h1>Hello, world!</h1>

		<!-- Require footer -->
		<?php require_once "footer.php"; ?>

	</body>

</html>