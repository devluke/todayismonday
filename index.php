<?php require_once "userpage.php" ?>

<!doctype html>
<html>

	<head>

		<!-- Require header -->
		<?php require_once "header.php"; ?>

	</head>

	<body>

		<!-- Require navbar -->
		<?php require_once "navbar.php"; ?>

		<br><br>

		<div class="container">

			<h1 class="display-4">Today is <?php echo date("l"); ?>,</h1>

			<h2><?php echo date("F d, Y."); ?></h2>

		</div>

		<!-- Require footer -->
		<?php require_once "footer.php"; ?>

	</body>

</html>